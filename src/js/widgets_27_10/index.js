import 'assets/style/index.less';
import 'assets/style/widget_edit_widget_27_10/index.less';
import 'assets/style/panels/widgets-panel/index.less';



var template = require("pug/widgets_27_10/index.pug");
//require("../jquery.nanoscroller.min.js");
$('.editable-widget-layout__category-content').masonry({
    // options
    itemSelector: '.editable-widget',
    //columnWidth: 190
});

$(".list-item--parent").on("click", function(e) {
    $(this).toggleClass("list-item--active")
});
$(".temperature-slider__slider").slider({
    range: "max",
    min: 0,
    max: 40,
    value: Number($("#temperature-slider__value").text()),
    slide: function(event, ui) {
        $("#temperature-slider__value").text(ui.value);
        $(this).slider('value', ui.value);
        console.log(ui.value)
    }
});

$('.editable-widget-layout__category-content').masonry({ 
    itemSelector: '.editable-widget',
});

setInterval(function() {
    var div = document.getElementsByTagName('body');
    div.innerHTML = template();

}, 1000);
