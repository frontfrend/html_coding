const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');


const config = {
    entry: {
        login: './src/js/login/index.js',
        widget_edit_widget_27_10: './src/js/widget_edit_widget_27_10/index.js',
        widget_edit_devices_27_10: './src/js/widget_edit_devices_27_10/index.js',
        widgets_27_10: './src/js/widgets_27_10/index.js',
        edit_profile: './src/js/edit_profile/index.js',
        editing_scripts: './src/js/editing_scripts/index.js',
        room: './src/js/room/index.js',
        grouping_devices: './src/js/grouping_devices/index.js',
        edit_side_panel: './src/js/edit_side_panel/index.js',
        widgets_script_editing: './src/js/widgets_script_editing/index.js',
        device_control_widgets: './src/js/device_control_widgets/index.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        //publicPath: "/assets/",
        filename: '[name].bundle.js'
    },
    module: {
        rules: [{
                test: /\.css$/,
                use: process.env.NODE_ENV !== 'production' ? ['style-loader', 'css-loader', 'postcss-loader'] : ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: 'css-loader?importLoaders=1!postcss-loader'
                })
            },
            {
                test: /\.less$/,
                use: process.env.NODE_ENV !== 'production' ? [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    'less-loader',
                    'postcss-loader'
                ] : ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: 'css-loader?importLoaders=1!less-loader!postcss-loader'
                })
            }, {
                test: /\.pug$/,
                use: {
                    loader: 'pug-loader'
                }
            },
            // {
            //     test: /\.(ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            //     use: {
            //         loader: 'file-loader'
            //     }
            // },
            // {
            //     test: /\.(png|jpg|svg)$/,
            //     loader: 'url-loader?limit=25000&outputPath=img/'
            // }
            {

                test: /\.(gif|png|jpg|jpeg|svg)($|\?)/,
                loaders: ['url-loader?limit=5000&hash=sha512&digest=hex&size=16&name=resources/[name]-[hash].[ext]']
            },
            {
                test: /\.(woff|woff2|eot|ttf)($|\?)/,
                loaders: ['url-loader?limit=5000&hash=sha512&digest=hex&size=16&name=resources/[name]-[hash].[ext]']
            }
        ]
    },
    plugins: [
        process.env.NODE_ENV == 'production' ? new ExtractTextPlugin("[name].bundle.css") : () => ({}),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './index.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'login.html',
            template: './src/pug/login/index.pug',
            chunks: ['login'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'widget_edit_widget_27_10.html',
            template: './src/pug/widget_edit_widget_27_10/index.pug',
            chunks: ['widget_edit_widget_27_10'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'widget_edit_devices_27_10.html',
            template: './src/pug/widget_edit_devices_27_10/index.pug',
            chunks: ['widget_edit_devices_27_10'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'widgets_27_10.html',
            template: './src/pug/widgets_27_10/index.pug',
            chunks: ['widgets_27_10'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'edit_profile.html',
            template: './src/pug/edit_profile/index.pug',
            chunks: ['edit_profile'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'editing_scripts.html',
            template: './src/pug/editing-scripts/index.pug',
            chunks: ['editing_scripts'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'room.html',
            template: './src/pug/room/index.pug',
            chunks: ['room'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'grouping_devices.html',
            template: './src/pug/grouping_devices/index.pug',
            chunks: ['grouping_devices'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'edit_side_panel.html',
            template: './src/pug/edit_side_panel/index.pug',
            chunks: ['edit_side_panel'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'widgets_script_editing.html',
            template: './src/pug/widgets_script_editing/index.pug',
            chunks: ['widgets_script_editing'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'device_control_widgets.html',
            template: './src/pug/device_control_widgets/index.pug',
            chunks: ['device_control_widgets'],
            inject: true
        })

    ],
    devServer: {
        host: 'localhost',
        port: 3000,
        historyApiFallback: true,
        hot: true,
        inline: true
    },
    resolve: {
        alias: {
            assets: path.resolve(__dirname, 'assets'),
            js: path.resolve(__dirname, 'src/js'),
            pug: path.resolve(__dirname, 'src/pug'),
        }
    }
}

// if (process.env.NODE_ENV.toString() == 'production') {
//     console.log(1111111111111111111111111111)
//     console.log(process.env.NODE_ENV)
//     config.plugins.push(
//         new ExtractTextPlugin("[name]/[name].bundle.css")
//     );

// }
module.exports = config;
