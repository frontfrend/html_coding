import 'assets/style/index.less';
import 'assets/style/panels/edit-device-panel/edit-device-panel.less';
import 'assets/style/panels/widgets-panel/index.less';
import 'assets/style/edit-side-panel/index.less';

var template = require("pug/edit_side_panel/index.pug");

$(".list-item--parent").on("click", function(e) {
    $(this).toggleClass("list-item--active")
});

setInterval(function() {
    var div = document.getElementsByTagName('body');
    div.innerHTML = template();


}, 1000);
