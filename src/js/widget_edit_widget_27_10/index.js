import 'assets/style/index.less';
import 'assets/style/widget_edit_widget_27_10/index.less';
import 'assets/style/panels/add-widget-panel/add-widget-panel.less';

var template = require("pug/widget_edit_widget_27_10/index.pug");
//require("../jquery.nanoscroller.min.js");
$('.editable-widget-layout__category-content').masonry({
    // options
    itemSelector: '.editable-widget',
    //columnWidth: 190
});



setInterval(function() {
    var div = document.getElementsByTagName('body');
    div.innerHTML = template();
    console.log($("#temperature-slider__value").text())
    $('.editable-widget-layout__category-content').masonry({
        // options
        itemSelector: '.editable-widget',
        //columnWidth: 190
    });
    $(".temperature-slider__slider").slider({
        range: "max",
        min: 0,
        max: 40,
        value: Number($("#temperature-slider__value").text()),
        slide: function(event, ui) {
            $("#temperature-slider__value").val(ui.value);
            $(this).slider('value', ui.value);
        }
    });

}, 1000);