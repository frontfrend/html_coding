import 'assets/style/index.less';
import 'assets/style/widget_edit_widget_27_10/index.less';
import 'assets/style/panels/edit-device-panel/edit-device-panel.less';

var template = require("pug/device_control_widgets/index.pug");
//require("../jquery.nanoscroller.min.js");
$('.editable-widget-layout__category-content').masonry({
    // options
    itemSelector: '.editable-widget',
    //columnWidth: 190
});

$(".list-item--parent").on("click", function (e) {
    if($(e.target).hasClass("bind") || $(e.target).parent().hasClass("bind")) return;
    $(this).toggleClass("list-item--active");
});

$(".bind").on("click", function (e) {
    $(this).toggleClass("bind--lock");

    const bindIcon = $(this).find($('.bind__icon')).first();
    bindIcon.toggleClass("fa-link");
    bindIcon.toggleClass("fa-chain-broken");

    if ($(this).hasClass("bind--lock")) {
        $(this).find($('.bind__text')).first().text("Отвязять");
    } else {
        $(this).find($('.bind__text')).first().text("Привязать");
    }
});




setInterval(function () {
    var div = document.getElementsByTagName('body');
    div.innerHTML = template();
    console.log($("#temperature-slider__value").text())
    $('.editable-widget-layout__category-content').masonry({
        // options
        itemSelector: '.editable-widget',
        //columnWidth: 190
    });
    $(".temperature-slider__slider").slider({
        range: "max",
        min: 0,
        max: 40,
        value: Number($("#temperature-slider__value").text()),
        slide: function (event, ui) {
            $("#temperature-slider__value").val(ui.value);
            $(this).slider('value', ui.value);
        }
    });

}, 1000);